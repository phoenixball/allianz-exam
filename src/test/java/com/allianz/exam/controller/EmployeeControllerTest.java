package com.allianz.exam.controller;

import com.allianz.exam.util.TokenUtil;
import org.apache.http.HttpHeaders;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.util.Strings;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
@Transactional
public class EmployeeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TokenUtil tokenUtil;

    @Test
    public void test_getAllEmployee_success_by_user_with_role_user() throws Exception {
        String accessToken = getAccessToken("user", "pass");

        MvcResult mvcResult = mockMvc.perform(get("/employee")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String userRole = getUserRole(accessToken);

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"Success\",\"data\":[{\"id\":1,\"firstName\":\"Harry\",\"lastName\":\"Potter\"},{\"id\":2,\"firstName\":\"Hermione\",\"lastName\":\"Granger\"},{\"id\":3,\"firstName\":\"Ron\",\"lastName\":\"Weasley\"}]}";

        assertEquals(expected, actual);
        assertEquals("USER", userRole);
    }

    @Test
    public void test_getAllEmployee_success_by_user_with_role_admin() throws Exception {
        String accessToken = getAccessToken("admin", "pass");

        MvcResult mvcResult = mockMvc.perform(get("/employee")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"Success\",\"data\":[{\"id\":1,\"firstName\":\"Harry\",\"lastName\":\"Potter\"},{\"id\":2,\"firstName\":\"Hermione\",\"lastName\":\"Granger\"},{\"id\":3,\"firstName\":\"Ron\",\"lastName\":\"Weasley\"}]}";

        String userRole = getUserRole(accessToken);

        assertEquals(expected, actual);
        assertEquals("ADMIN", userRole);
    }

    @Test
    public void test_getAllEmployee_unauthorized_invalid_user() throws Exception {
        String accessToken = getAccessToken("invalid_user", "1234");

        mockMvc.perform(get("/employee")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test_getAllEmployee_unauthorized_invalid_access_token() throws Exception {
        getAccessToken("user", "pass");

        mockMvc.perform(get("/employee")
                .header(HttpHeaders.AUTHORIZATION, "Bearer dgkljnarewkjwetrnegd")
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());

        getAccessToken("user", "pass");
    }

    @Test
    public void test_getEmployeeById_success_by_user_with_role_user() throws Exception {
        String accessToken = getAccessToken("user", "pass");

        MvcResult mvcResult = mockMvc.perform(get("/employee/1")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"Success\",\"data\":{\"id\":1,\"firstName\":\"Harry\",\"lastName\":\"Potter\"}}";

        String userRole = getUserRole(accessToken);

        assertEquals(expected, actual);
        assertEquals("USER", userRole);
    }

    @Test
    public void test_getEmployeeById_invalid_employee_id() throws Exception {
        String accessToken = getAccessToken("user", "pass");

        MvcResult mvcResult = mockMvc.perform(get("/employee/9")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"No employee id: {9}\"}";

        assertEquals(expected, actual);
    }

    @Test
    public void test_getEmployeeById_unauthorized_invalid_user() throws Exception {
        String accessToken = getAccessToken("invalid_user", "1234");

        mockMvc.perform(get("/employee/1")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test_getEmployeeById_unauthorized_invalid_access_token() throws Exception {
        getAccessToken("user", "pass");

        mockMvc.perform(get("/employee/1")
                .header(HttpHeaders.AUTHORIZATION, "Bearer dgkljnarewkjwetrnegd")
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test_createEmployee_success_by_user_with_role_admin() throws Exception {
        String accessToken = getAccessToken("admin", "pass");

        MvcResult mvcResult = mockMvc.perform(post("/employee")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .content("{\n" +
                        "    \"firstName\": \"Winnie\",\n" +
                        "    \"lastName\": \"Pooh\"\n" +
                        "}")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"Success\",\"data\":{\"id\":4,\"firstName\":\"Winnie\",\"lastName\":\"Pooh\"}}";

        String userRole = getUserRole(accessToken);

        assertEquals(expected, actual);
        assertEquals("ADMIN", userRole);
    }

    @Test
    public void test_createEmployee_unauthorized_invalid_user() throws Exception {
        String accessToken = getAccessToken("invalid_user", "1234");

        mockMvc.perform(post("/employee")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test_createEmployee_unauthorized_invalid_access_token() throws Exception {
        getAccessToken("user", "pass");

        mockMvc.perform(post("/employee")
                .header(HttpHeaders.AUTHORIZATION, "Bearer dgkljnarewkjwetrnegd")
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test_updateEmployee_success_by_user_with_role_admin() throws Exception {
        String accessToken = getAccessToken("admin", "pass");

        MvcResult mvcResult = mockMvc.perform(put("/employee/3")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .content("{\n" +
                        "    \"firstName\": \"Winnie\",\n" +
                        "    \"lastName\": \"Pooh\"\n" +
                        "}")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"Success\",\"data\":{\"id\":3,\"firstName\":\"Winnie\",\"lastName\":\"Pooh\"}}";

        String userRole = getUserRole(accessToken);

        assertEquals(expected, actual);
        assertEquals("ADMIN", userRole);
    }

    @Test
    public void test_updateEmployee_invalid_employee_id() throws Exception {
        String accessToken = getAccessToken("admin", "pass");

        MvcResult mvcResult = mockMvc.perform(put("/employee/9")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .content("{\n" +
                        "    \"firstName\": \"Winnie\",\n" +
                        "    \"lastName\": \"Pooh\"\n" +
                        "}")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"No employee id: {9}\"}";

        assertEquals(expected, actual);
    }

    @Test
    public void test_updateEmployee_unauthorized_invalid_user() throws Exception {
        String accessToken = getAccessToken("invalid_user", "1234");

        mockMvc.perform(put("/employee/3")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test_updateEmployee_unauthorized_invalid_access_token() throws Exception {
        getAccessToken("user", "pass");

        mockMvc.perform(put("/employee/3")
                .header(HttpHeaders.AUTHORIZATION, "Bearer dgkljnarewkjwetrnegd")
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test_deleteEmployee_success_by_user_with_role_admin() throws Exception {
        String accessToken = getAccessToken("admin", "pass");

        MvcResult mvcResult = mockMvc.perform(delete("/employee/3")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"Success\"}";

        String userRole = getUserRole(accessToken);

        assertEquals(expected, actual);
        assertEquals("ADMIN", userRole);
    }

    @Test
    public void test_deleteEmployee_invalid_employee_id() throws Exception {
        String accessToken = getAccessToken("admin", "pass");

        MvcResult mvcResult = mockMvc.perform(delete("/employee/9")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();
        String expected = "{\"message\":\"No employee id: {9}\"}";

        assertEquals(expected, actual);
    }

    @Test
    public void test_deleteEmployee_forbidden_normal_user() throws Exception {
        String accessToken = getAccessToken("user", "pass");

        mockMvc.perform(delete("/employee/3")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_deleteEmployee_unauthorized_invalid_user() throws Exception {
        String accessToken = getAccessToken("invalid_user", "1234");

        mockMvc.perform(delete("/employee/3")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void test_deleteEmployee_unauthorized_invalid_access_token() throws Exception {
        getAccessToken("user", "pass");

        mockMvc.perform(delete("/employee/3")
                .header(HttpHeaders.AUTHORIZATION, "Bearer dgkljnarewkjwetrnegd")
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    private String getAccessToken(String username, String password) throws Exception {
        MvcResult result = mockMvc.perform(post("/oauth/token")
                .header(HttpHeaders.AUTHORIZATION,
                "Basic " + Base64Utils.encodeToString("clientId:clientSecret".getBytes()))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
                        new BasicNameValuePair("grant_type", "password"),
                        new BasicNameValuePair("username", username),
                        new BasicNameValuePair("password", password)
                )))))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        String resultString = result.getResponse().getContentAsString();

        try {
            JacksonJsonParser jsonParser = new JacksonJsonParser();
            return jsonParser.parseMap(resultString).get("access_token").toString();
        } catch (Exception e) {
            return Strings.EMPTY;
        }
    }

    @SuppressWarnings("unchecked")
    private String getUserRole(String accessToken) throws Exception {
        Map<String, Object> tokenMap = tokenUtil.decodeJWT(accessToken);
        List<String> authorities = (List<String>) tokenMap.get("authorities");
        return authorities.get(0);
    }
}