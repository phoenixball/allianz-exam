package com.allianz.exam.service;

import com.allianz.exam.entity.Employee;
import com.allianz.exam.model.request.EmployeeRequest;
import com.allianz.exam.repository.EmployeeRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Spy
    @InjectMocks
    private EmployeeService employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Test
    public void test_getAllEmployee_success() {
        employeeService.getAllEmployee();

        verify(employeeRepository).findAll();
    }

    @Test
    public void test_getEmployeeById_success() {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setFirstName("Winnie");
        employee.setLastName("Pooh");

        when(employeeRepository.findById(1L)).thenReturn(Optional.of(employee));

        Employee employeeById = employeeService.getEmployeeById(1L);

        assertThat(employeeById, is(employee));
    }

    @Test
    public void test_getEmployeeById_no_employee_id() {
        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("No employee id: {1}");

        when(employeeRepository.findById(1L)).thenReturn(Optional.empty());

        employeeService.getEmployeeById(1L);
    }

    @Test
    public void test_createEmployee_success() {
        EmployeeRequest employeeRequest = EmployeeRequest.builder().firstName("Winnie").lastName("Pooh").build();

        employeeService.createEmployee(employeeRequest);

        Employee employee = new Employee();
        employee.setFirstName(employeeRequest.getFirstName());
        employee.setLastName(employeeRequest.getLastName());

        verify(employeeRepository).save(employee);
    }

    @Test
    public void test_updateEmployee_success() {
        Employee employee = new Employee();

        employeeService.updateEmployee(employee);

        verify(employeeRepository).save(employee);
    }

    @Test
    public void test_deleteEmployee_success() {
        employeeService.deleteEmployee(1L);

        verify(employeeRepository).deleteById(1L);
    }
}