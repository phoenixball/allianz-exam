package com.allianz.exam.service;

import com.allianz.exam.entity.User;
import com.allianz.exam.repository.UserRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void test_loadUserByUsername_success() {
        String role = "ADMIN";
        String username = "username";
        String password = "$2a$10$z0UzKtgFghfkyDrj84r6X.x6yUf9.jcJALXx2JVWbW5hK1S1uuLmW";

        User userEntity = new User();
        userEntity.setRole(role);
        userEntity.setUsername(username);
        userEntity.setPassword(password);

        Optional<User> user = Optional.of(userEntity);
        when(userRepository.findByUsername("username")).thenReturn(user);

        UserDetails userDetails = userService.loadUserByUsername("username");

        GrantedAuthority authority = new SimpleGrantedAuthority(role);
        org.springframework.security.core.userdetails.User expected = new org.springframework.security.core.userdetails.User(username, password, Arrays.asList(authority));

        assertThat(userDetails, is(expected));
    }

    @Test
    public void test_loadUserByUsername_user_not_found() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("User not found: username");

        when(userRepository.findByUsername("username")).thenReturn(Optional.empty());

        userService.loadUserByUsername("username");
    }
}