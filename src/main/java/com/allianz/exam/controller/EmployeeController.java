package com.allianz.exam.controller;

import com.allianz.exam.entity.Employee;
import com.allianz.exam.model.request.EmployeeRequest;
import com.allianz.exam.model.response.ApiResponse;
import com.allianz.exam.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    public ApiResponse<List<Employee>> getAllEmployee() {
        try {
            List<Employee> allEmployee = employeeService.getAllEmployee();
            return ApiResponse.<List<Employee>>builder().data(allEmployee).message("Success").build();
        } catch (Exception e) {
            String message = e.getMessage();
            return ApiResponse.<List<Employee>>builder().message(message).build();
        }
    }

    @GetMapping(value = "/{employee_id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    public ApiResponse<Employee> getEmployeeById(@PathVariable(name = "employee_id") Long employeeId) {
        try {
            Employee employeeById = employeeService.getEmployeeById(employeeId);
            return ApiResponse.<Employee>builder().data(employeeById).message("Success").build();
        } catch (Exception e) {
            String message = e.getMessage();
            return ApiResponse.<Employee>builder().message(message).build();
        }
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    public ApiResponse<Employee> createEmployee(@RequestBody EmployeeRequest employeeRequest) {
        try {
            Employee employee = employeeService.createEmployee(employeeRequest);
            return ApiResponse.<Employee>builder().data(employee).message("Success").build();
        } catch (Exception e) {
            String message = e.getMessage();
            return ApiResponse.<Employee>builder().message(message).build();
        }
    }

    @PutMapping(value = "/{employee_id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    public ApiResponse<Employee> updateEmployee(@PathVariable(name = "employee_id") Long employeeId, @RequestBody EmployeeRequest employeeRequest) {
        try {
            Employee employeeById = employeeService.getEmployeeById(employeeId);
            employeeById.setFirstName(employeeRequest.getFirstName());
            employeeById.setLastName(employeeRequest.getLastName());
            Employee updatedEmployee = employeeService.updateEmployee(employeeById);
            return ApiResponse.<Employee>builder().data(updatedEmployee).message("Success").build();
        } catch (Exception e) {
            String message = e.getMessage();
            return ApiResponse.<Employee>builder().message(message).build();
        }
    }

    @DeleteMapping(value = "/{employee_id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ApiResponse<Void> deleteEmployee(@PathVariable(name = "employee_id") Long employeeId) {
        try {
            employeeService.getEmployeeById(employeeId);
            employeeService.deleteEmployee(employeeId);
            return ApiResponse.<Void>builder().message("Success").build();
        } catch (Exception e) {
            String message = e.getMessage();
            return ApiResponse.<Void>builder().message(message).build();
        }
    }
}
