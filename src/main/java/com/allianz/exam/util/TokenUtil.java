package com.allianz.exam.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import java.util.Map;


@Slf4j
@Component
public class TokenUtil {
    private final ObjectMapper objectMapper;

    public TokenUtil(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public Map<String, Object> decodeJWT(String jwtToken) throws Exception {
        try {
            String[] splitString = jwtToken.split("\\.");

            Base64 base64Url = new Base64(true);
            String payloadJson = new String(base64Url.decode(splitString[1]));

            return objectMapper.readValue(payloadJson, new TypeReference<Map<String, Object>>() {});
        } catch (Exception e) {
            log.error("decodeJWT error: {}", e.getMessage(), e);
            throw new RuntimeException("Invalid token format");
        }
    }
}
