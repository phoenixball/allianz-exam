-- encrypted password is 'pass'
insert into `user` (username, password, `role`) values('admin','$2a$10$z0UzKtgFghfkyDrj84r6X.x6yUf9.jcJALXx2JVWbW5hK1S1uuLmW', 'ADMIN');
insert into `user` (username, password, `role`) values('user','$2a$10$z0UzKtgFghfkyDrj84r6X.x6yUf9.jcJALXx2JVWbW5hK1S1uuLmW', 'USER');

insert into employee
values(1,'Harry', 'Potter');

insert into employee
values(2,'Hermione', 'Granger');

insert into employee
values(3,'Ron', 'Weasley');