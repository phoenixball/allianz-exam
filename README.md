# Backend Spring REST API coding exercise

Goal is to create a simple Spring Boot application with REST API services provider.

Java 8 or newer and Spring Boot must be used.

Feel free to use Maven or Gradle with any libraries, if and as needed.

##

## Documentation
#### Technology summary
    - Spring Boot, Java 8
    - Spring Data JPA
    - Spring Security
    - Lombok
    - H2 Database
    - Docker
    - JUnit, Mockito
    - Maven
    

#### Running docker container locally

1. Create JAR of application
    ```bash
    ./mvnw clean package
    ```
2. Build docker image
    ```bash
    docker build -t app-docker .
    ```
3. Run docker image
    ```bash
    docker run -p 8080:8080 app-docker
   
   .   ____          _            __ _ _
   /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
   ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
   \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
   '  |____| .__|_| |_|_| |_\__, | / / / /
   =========|_|==============|___/=/_/_/_/
   :: Spring Boot ::        (v2.0.4.RELEASE)
   ......
   --- [main] : Started Application in 5.24 seconds (JVM running for ..
    ```
## 
#### Database
- H2 Console http://localhost:8080/h2-console
- Table `user`

    ID | USERNAME | ROLE
    ------------- | ------------- | -------------
    1  | admin    | ADMIN
    2  | user     | USER
    
- Table `employee`

    ID | FIRST_NAME | LAST_NAME
    ------------- | ------------- | -------------
    1  | Harry    | Potter
    2  | Hermione | Granger
    3  | Ron      | Weasley

#### API Specs

- Get OAuth Token
    - URL: `http://localhost:8080/oauth/token`
    - METHOD: `POST`
    
   ```bash
   curl --location --request POST 'http://localhost:8080/oauth/token' \
   --header 'Authorization: Basic Y2xpZW50SWQ6Y2xpZW50U2VjcmV0' \
   --form 'grant_type="password"' \
   --form 'username="admin"' \
   --form 'password="pass"'
   ```
   
- Get All Employee
    - URL: `http://localhost:8080/employee`
    - METHOD: `GET`
    
   ```bash
    curl --location --request GET 'http://localhost:8080/employee' \
    --header 'Authorization: Bearer {oAuthToken}' \
   ```
   
- Get Employee by ID
    - URL: `http://localhost:8080/employee/{employee_id}`
    - METHOD: `GET`
    
   ```bash
    curl --location --request GET 'http://localhost:8080/employee/{employee_id}' \
    --header 'Authorization: Bearer {oAuthToken}' \
   ```
   
- Create Employee
    - URL: `http://localhost:8080/employee`
    - METHOD: `POST`
    
   ```bash
    curl --location --request POST 'http://localhost:8080/employee' \
    --header 'Authorization: Bearer {oAuthToken}' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "firstName": "Winnie",
        "lastName": "Pooh"
    }'
   ```
   
- Update Employee
    - URL: `http://localhost:8080/employee/{employee_id}`
    - METHOD: `PUT`
    
   ```bash
    curl --location --request PUT 'http://localhost:8080/employee/{employee_id}' \
    --header 'Authorization: Bearer {oAuthToken}' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "firstName": "Winnie",
        "lastName": "Pooh"
    }'
   ```
   
- Delete Employee
    - URL: `http://localhost:8080/employee/{employee_id}`
    - METHOD: `DELETE`
    
   ```bash
    curl --location --request DELETE 'http://localhost:8080/employee/{employee_id}' \
    --header 'Authorization: Bearer {oAuthToken}' \
   ```

## Requirements

1. Scaffold the project with data source connection (any database but in memory one is preferred).
2. Create API for authentication, check User existing and return generated token (Use this token to secure APIs on point 3-7).
3. Create API for retrieving all employee.
4. Create API for retrieving one employee by ID.
5. Create API for saving one employee.
6. Create API for modifying one employee.
7. Create API for deleting one employee by ID.
8. Unit testing.
9. Integration testing.
10. Swagger is a plus.
11. Liquibase is a plus.
12. MapStruct is a plus.
13. Docker is a plus.

## Important note
A high attention to the quality of the application will be paid: project structure, clean code, best practices, comments, ...

It is much preferred to do not cover all requirements but provide a good code quality than doing everything and using all technologies but providing a code not readable, an application not secured, ...

## Result
When finished, please provide a public link to your repository. 


## 

